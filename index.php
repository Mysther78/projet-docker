<?php
session_start();
?>
<?php 
if(isset($_SESSION['token']) && isset($_SESSION['username'])) {

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Index - MicroAir</title>
    </head>
    <body id='body2'>
        <div class="grid-container">
            
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
            

            

            <main class="main">
            <div class="main-header">
                <div class="main-header__heading">
                <h3>Qui sommes-nous ?</h3>
                	<p>
Nous sommes un groupe de 6 étudiants en DUT informatique a l'IUT de Vélizy de l'Université Versailles Saint Quentin en Yvelines. Nous avons réalisé ce projet dans le cadre du projet de fin de deuxième année. Bonne visite sur notre site, Cordialement, l'équipe MicroAir</p>
					
                </div>
                
            </div>

<!--            <div class="main-overview">
                <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            </div>
-->
            <div class="main-cards">
                <div class="card">
                <h3>Contexte & Explications</h3>
                <p>Le Software as a Service (SaaS) est un modèle de distribution de software dans lequel une organisation met à disposition le logiciel à ses clients via Internet. Le SaaS est une 					catégorie du Cloud Computing.<br>
					Ce principe ôte le besoin d’installer et exécuter le logiciel en local, sur une machine, afin d’éviter les restrictions liées à l’OS individuel.</p>
					<p>La technologie docker permet de répondre à ce besoin. A l’instar de la virtualisation (utilisation de machines virtuelles), la technologie docker permet de déployer des applications 						dans des environnements indépendants de l’OS, dans des “conteneurs”. Ces conteneurs contiennent uniquement un OS basique (la technologie elle-même est basée sur le noyau Linux), ainsi que 					l’application à déployer et tout ce qui lui permet de fonctionner correctement : ses dépendances.<br>
					On distingue le dockerfile, un fichier texte contenant des informations qui permettent la construction d’une image docker. Une image docker sera déployée dans un conteneur prévu à cet effet.</p>
					<p>Dans le cadre du concept de SaaS, la finalité de notre projet est de <b>permettre à un client de déployer son application de manière Dockerisée</b>, dans un conteneur isolé, donc qui 						ne possède pas de dépendances liées à l’OS.</p>
					<p><b>La finalité du projet</b> : Aboutir au déploiement de l’application du client dans un environnement portable, mobile, dépourvu de dépendances non nécessaires, et pouvant être 						exécuté à partir de n’importe quel système d’exploitation à partir du cloud : docker.</p>
                </div>
                <div class="card">
                <h3>Notre application</h3>
				<p>Est une application web  pourvue d’une interface, qui gère :</p>
				<ul>
							<li>D’une part <b>la connexion au cloud Docker (Dockerhub)</b>, pour pouvoir télécharger une image pré-existante (API pour Dockerhub, gérant la connexion, push, et pull une image Docker)</li> <br>
							<li>A partir de cette image, la <b>création d’images docker</b> qui, lors de leur déploiement, permettront l’installation d’un serveur web (apache…), php, et un service base de données (postgres par exemple, que l’application cliente utilisera), et qui permettront au client de télécharger son code source grâce à Git.</li> <br>
							<li>Plus précisément, nous mettrons en œuvre <b>deux conteneurs</b> : un conteneur contenant le <b>serveur web</b>, php, et possiblement le code source du client, et un autre contenant la <b>base de données</b> nécessaire à l’application cliente. Notre projet se chargera de la communication entre ces deux conteneurs.</li>
						</ul>
                </div>
            </div>
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>

<?php 
}
else {
	header("Location: login.php");
}
?>
