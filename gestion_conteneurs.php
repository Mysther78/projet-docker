<?php
session_start();
date_default_timezone_set("Europe/Paris");
$socket = fsockopen('unix:///var/run/docker.sock',0, $errno, $errstr,20);
if(!$socket) {
	echo "erreur" . $errno . " " . $errstr	;
}
else {
	$out = "GET /containers/json?all=1 HTTP/1.1\r\n";
	$out .= "Host: \r\n";
	$out .= "Content-Type: application/json\r\n";
	$out .= "Connection: Close\r\n\r\n";
	fwrite($socket,$out);

	$header = "";
	$data = "";
	$isHeader = True;

	while(!feof($socket)) {
		$output = fgets($socket,2048); 		//On recupère ce qu'il y a dans le socket
		$output = str_replace(array("\n", "\r"), '',$output); //On elève les "\r" et "\n"


		if(substr($output, 0, 2) == "[{") 	// Si le début de la phrase est "[{", c'est le début des données à récuperer
			$isHeader = False;


		if($isHeader) 						//Si l'output est un header, on l'enregistre dans la varible header
			$header .= $output;
		else 								//Si l'output sont les données, on l'enregistre dans la varible data
			$data .= $output;
	}
	

	// $data{strlen($data)-1} Permet de récupérer le dernier caracère d'une string
	while($data{strlen($data)-1} ==! "]" and strlen($data) > 0)
		$data = substr($data, 0, -1); //Suppression du dernier caractère

	$json = json_decode($data,true);
	fclose($socket);
//	echo "<pre>";
//	print_r($json);
//	echo "</pre>";
}?>

<?php 
if(isset($_SESSION['token']) && isset($_SESSION['username'])) {

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Conteneurs - MicroAir</title>
    </head>
    <body id='body2'>
        <div class="grid-container">
        <div hidden id='popup-infos'> </div>
            
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
            <main class="main">
            <div class="main-header conteneurs">
                <div class="main-header__heading">
					<div id='gestion_conteneur'>
						<div id='action_conteneur'>
							<div>START</div><div>STOP</div><div>SUPPRIMER</div>
						</div>
						<table cellpadding=0 cellspacing=0>
							<tr><td></td><td>Nom</td><td>Status</td><td>Image</td><td>Date de création</td><td>Adresse IP</td><td>Port(HOST:CONTAINER)</td></tr>
							<?php 
							
							for($i = 0 ; $i < sizeof($json) ; $i++) {
								echo "<tr>";
								$name = ltrim($json[$i]['Names'][0],'/'); // permet de supprimer le premier slash / qui va nous faire chier pour plus tard
								echo "<td><input type='checkbox' nb='{$i}' name='{$name}'/></td>";
								echo "<td>{$name}</td>";
								if(preg_match("#Created#",$json[$i]['Status'])) {
									$status = "CRÉE";
								}
								else if(preg_match("#Exited#",$json[$i]['Status'])) {
									$status = "OFF";
								}
								else if(preg_match("#Up#",$json[$i]['Status'])) {
									$status = "ON";
								}
								echo "<td><div id='status' value='{$status}'>{$status}</div></td>";
				//				echo "<td>{$json[$i]['Status']}</td>";
								echo "<td>{$json[$i]['Image']}</td>";
								$created = date("d/m/Y h:i:s",$json[$i]['Created']);
								echo "<td>{$created}</td>";
								echo "<td>{$json[$i]['NetworkSettings']['Networks']['bridge']['IPAddress']}</td>";
								echo "<td>{$json[$i]['Ports'][0]['PublicPort']}:{$json[$i]['Ports'][0]['PrivatePort']}</td>";
								echo "</tr>";
								
							}
							
							
							?>
							
							
						</table>
					
					</div>
                </div>
                
            </div>
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>
<script>
	
	var stats = document.querySelectorAll('#status');
	var checkboxs = document.querySelectorAll('input[type=checkbox]');
	for(var i = 0 ; i < checkboxs.length ; i++) {
		checkboxs[i].checked = false;
	}
	var action_conteneur = document.getElementById('action_conteneur');
	var xhr = new XMLHttpRequest();
	var container_name;
	var st; // status 
	xhr.onreadystatechange = callback_gestionConteneurs;
//	console.log(action_conteneur.childNodes);
	for(var i = 0; i < stats.length ; i++) {
		if(stats[i].innerHTML == 'CRÉE') {
			stats[i].style.backgroundColor = "lightblue";
		}
		else if(stats[i].innerHTML == 'OFF') {
			stats[i].style.backgroundColor = "orangered";
		}
		else if(stats[i].innerHTML == "ON") {
			stats[i].style.backgroundColor = "green";
		}
	}
	var clicked = false;
	var nb = -1; // == ligne cochée , initialisation à -1
	for(i = 0 ; i < checkboxs.length ; i++) {
		checkboxs[i].addEventListener("click",function() {		
			/* Permet de ne gerer qu'un seul conteneur  la fois, pour le moment on va faire simple */
			if(this.checked == true && clicked == false && nb == -1) {
				clicked = true;
				nb = this.getAttribute("nb");
				st = stats[nb].innerHTML;	
//				console.log(st);
				if(st == "OFF" || st == "CRÉE") {
					action_conteneur.childNodes[1].style.backgroundColor = "green";
//					action_conteneur.style.cursor = "pointer";
					action_conteneur.childNodes[1].style.cursor = "pointer";
					action_conteneur.childNodes[2].style.backgroundColor = "rgba(255,69,0,0.4)";
					action_conteneur.childNodes[3].style.backgroundColor = "orangered";
					action_conteneur.childNodes[3].style.cursor = "pointer";
				}
				else if(st == "ON"){
					action_conteneur.childNodes[2].style.backgroundColor = "orangered";
					action_conteneur.childNodes[3].style.backgroundColor = "orangered";
					action_conteneur.childNodes[2].style.cursor = "pointer";
					action_conteneur.childNodes[3].style.cursor = "pointer";
					action_conteneur.childNodes[1].style.backgroundColor = "rgba(0,230,0,0.4)";
				}
				
				
				
//				action_conteneur.childNodes[1].style.backgroundColor = "green";
//				action_conteneur.childNodes[2].style.backgroundColor = "orangered";
//				action_conteneur.childNodes[3].style.backgroundColor = "orangered";
//				action_conteneur.style.cursor = "pointer";
				container_name = this.name;
							
//				console.log(nb);
			}
			else {
				clicked = false;
				nb = -1;
			}
			
			/* ********* FIN CLIC *******/
			
			
			/* deux boucles qui permettent de disabled ou non les cases une fois coche */
			if(nb != -1) {
				for(var j = 0 ; j < checkboxs.length ; j++) {
					if(j != nb) {
						checkboxs[j].disabled = true;
					}
				}
			}
			else {
				for(var j = 0 ; j < checkboxs.length ; j++) {
					if(j != nb) {
						checkboxs[j].disabled = false;
						action_conteneur.childNodes[1].style.backgroundColor = "rgba(0,230,0,0.4)";
						action_conteneur.childNodes[2].style.backgroundColor = "rgba(255,69,0,0.4)";
						action_conteneur.childNodes[3].style.backgroundColor = "rgba(255,69,0,0.4)";
						action_conteneur.style.cursor = "not-allowed";
					}
				}
			}
			
		});
	}
	action_conteneur.childNodes[1].addEventListener("click",function() {
			if(nb!=-1) { // on verifie qu'un conteneur est bien choisi
				if(st == "OFF" || st == "CRÉE") {
//					console.log(container_name);
					xhr.open("POST","js/conteneurs.php");
					xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					to_send = "container_name="+container_name+"&start";
//					console.log(to_send);
					xhr.send(to_send);
					document.body.style.cursor = "wait";
				}
			}
		});
	action_conteneur.childNodes[2].addEventListener("click",function() {
		if(nb != -1) { // on verifie que le conteneur est bien choisi
			if(st == "ON") { // status ON
				xhr.open("POST","js/conteneurs.php");
					xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					to_send = "container_name="+container_name+"&stop";	
//					console.log(to_send);
					xhr.send(to_send);
					document.body.style.cursor = "wait";
			}
		}
	});
	action_conteneur.childNodes[3].addEventListener("click",function() {
		if(nb != -1) { // on verifie que le conteneur est bien choisi
			xhr.open("POST","js/conteneurs.php");
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			to_send = "container_name="+container_name+"&delete";
//			console.log(to_send);
			xhr.send(to_send);
			document.body.style.cursor = "wait";
		}
	});
	
	
	var intervals = 0;
	var intervalId;
	var popup = document.getElementById("popup-infos");
	function fpopup() {
		if(intervals == 3) {
			clearInterval(intervalId);
			window.location.href = window.location;
			document.body.style.cursor = "pointer";
		}	
		popup.style.display = "block";
		popup.style.backgroundColor = "green";		
		intervals += 1;
	}
	
	
function callback_gestionConteneurs() {
//	console.log(xhr);
	if(xhr.status == 200 && xhr.readyState == 4) {
		if(xhr.responseText == "on") {
			intervalId = setInterval(fpopup, 100);
			popup.innerHTML = "Conteneur crée avec succès";
		}
		else if(xhr.responseText == "off") {
			intervalId = setInterval(fpopup, 100);
			popup.innerHTML = "Conteneur arrêté avec succès";
			
		}
		else if(xhr.responseText == "delete") {
			intervalId = setInterval(fpopup, 100);
			popup.innerHTML = "Conteneur supprimé avec succès";
			
		}
//				window.location.href = window.location;
		
		
//			console.log("ok xhr");
		
	}
}
	

</script>
<?php 
}
else {
	header("Location: login.php");
}
?>

