<?php

$ch = curl_init();
//$idNetwork = "e66e4f1a7541c1263f44d3f1ce2e4386122680f5716bfe2bbabd0793ed343f4b";
curl_setopt($ch, CURLOPT_URL, 'http://v1.39/containers/json?all=1');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
//curl_setopt($ch, CURLOPT_GET);

$headers = array();
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
	echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
$json = json_decode($result,true);

$networkName = "networktest2";
$containersConnected = [];
for($i = 0 ; $i < sizeof($json) ; $i++) {
	if(isset($json[$i]["NetworkSettings"]["Networks"][$networkName])) {
		$containersConnected[] = ltrim($json[$i]['Names'][0],"/");
	}
}

echo "<pre>";
print_r($containersConnected);
echo json_encode($containersConnected);
echo "</pre>";
?>
