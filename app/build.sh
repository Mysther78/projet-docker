#!/bin/bash
#Il faut que le docker login soit fait avant tout !

csv_exec_file="./exec.csv"
csv_temp=$csv_exec_file"~"
cp -f ${csv_exec_file} ${csv_temp}
rm -f ${csv_exec_file}


#while IFS=';' read path_dockerfile username image_name path_log; do
#  log=${path_log}"_"$(date +%s)".log"
#  touch ${log}
#  echo "=== docker build ===" >>${log} 2>&1
#  docker build "$path_dockerfile" -t "$username"/"$image_name" >>${log} 2>&1
#  echo "=== docker push ===" >>${log} 2>&1
#  docker push "$username"/"$image_name" >>${log} 2>&1
#done < "$csv_temp"

while IFS=';' read path_root_server username image_name; do
  path_dockerfile=${path_root_server}"/app/users/"${username}"/docker/"
  log=${path_root_server}"/app/users/"${username}"/"${image_name}"_"$(date +%s)".log"
  touch ${log}
  echo "=== docker build ===" >>${log} 2>&1
  docker build "$path_dockerfile" -t "$username"/"$image_name" >>${log} 2>&1
  echo "=== docker push ===" >>${log} 2>&1
  docker push "$username"/"$image_name" >>${log} 2>&1
  rm $path_dockerfile*.sql
done < "$csv_temp"

rm -f "$csv_temp"
