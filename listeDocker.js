xhr = new XMLHttpRequest();
xhr.open('POST','listeDocker.php');
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

//console.log('chargement...');
xhr.send(null);

xhr.onreadystatechange = callback_listeDocker;


function callback_listeDocker() {
//	console.log(xhr);
	if(xhr.readyState == 4 && xhr.status == 200) {
		//rep = JSON.parse(xhr.responseText);
		result = JSON.parse(xhr.responseText);
//		console.log(result.results);
		contenu = document.querySelector('.main-header__heading');
		loading = document.getElementById('loading');
		loading.style.display = 'none';
		for(i = 0 ; i < result.results.length ; i++ ) {
//			console.log(result.results[i]);
			
			/* DECLARATION DES ELEMENTS HTML */
			main_container_liste_docker = document.createElement('div');
			main_container_liste_docker.setAttribute('id','main-container-liste-docker');
			element_docker = document.createElement('div');
			element_docker.setAttribute('class','element-docker');
			
			//Partie gauche
			gauche = document.createElement('div');
			gauche.setAttribute('id','gauche');
			title = document.createElement('div');
			title.setAttribute('class','title');
			last_update = document.createElement('div');
			last_update.setAttribute('class','last_update');
			label_dockerhub = document.createElement('div');
			label_dockerhub.setAttribute('class','label-dockerhub');
			labelDockerHub = document.createTextNode("DockerHub");
			
			//Partie droite
			droite = document.createElement('div');
			droite.setAttribute('id','droite');
			download = document.createElement('div');
			download.setAttribute('class','download');
			startBtn = document.createElement('span');
			startBtn.setAttribute('nb2',i);
//			startBtn.setAttribute('class','info');
//			startBtn.textContent = "ℹ";
			deleteBtn = document.createElement('span');
			deleteBtn.setAttribute('nb2',i);
//			deleteBtn.setAttribute('class','supprimer-image');
//			deleteBtn.textContent = "✖";
			
			
//			img = document.createElement('img');
//			img.setAttribute('src','wallpaper/download.png');
//			img.setAttribute('alt','download_docker');
//			img.setAttribute('class','download_img');
			//console.log(result.results[i]['user']);
			username = document.createTextNode(result.results[i]['user'] + ' / ' + result.results[i]['name']);
			lastUpdate = document.createTextNode(new Date(result.results[i]['last_updated']).toLocaleString());

			/* FIN DECLARATION ELEMENTS HTML */
			
			/* AJOUT DES ELEMENTS HTML */
			contenu.appendChild(main_container_liste_docker);
			main_container_liste_docker.appendChild(element_docker);
			element_docker.appendChild(gauche);
			gauche.appendChild(title);
			title.appendChild(username);
			gauche.appendChild(label_dockerhub);
			label_dockerhub.appendChild(labelDockerHub);
			gauche.appendChild(last_update);
			last_update.appendChild(lastUpdate);
			element_docker.appendChild(droite);
			droite.appendChild(download);
			download.appendChild(startBtn);
			download.appendChild(deleteBtn);
			
		}
		
		
	}


}
