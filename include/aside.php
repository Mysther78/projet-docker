<aside class="sidenav">
	<div class="sidenav__close-icon">
		<i class="fas fa-times sidenav__brand-close"></i>
	</div>
	<ul class="sidenav__list">
	<a href='index.php'><li <?php if($_SERVER['REQUEST_URI'] == "/projet-docker/index.php") { echo "class=active"; }?> >ACCUEIL</li></a>
	<a href='creation.php'><li <?php if($_SERVER['REQUEST_URI'] == "/projet-docker/creation.php") { echo "class=active"; }?>>CREATION D'IMAGES</li></a>
	<a href='gestion_images.php'><li <?php if($_SERVER['REQUEST_URI'] == "/projet-docker/gestion_images.php") { echo "class=active"; }?>>GESTION IMAGES</li>
	<a href='gestion_conteneurs.php'><li <?php if($_SERVER['REQUEST_URI'] == "/projet-docker/gestion_conteneurs.php") { echo "class=active"; }?>>GESTION CONTENEURS</li></a>
	<a href='gestion_network.php'><li <?php if($_SERVER['REQUEST_URI'] == "/projet-docker/gestion_network.php") { echo "class=active"; }?>>GESTION NETWORK</li></a>
	<a href='log_build.php'><li <?php if($_SERVER['REQUEST_URI'] == "/projet-docker/log_build.php") { echo "class=active"; }?>>LOGS</li></a>
	</ul>
</aside>
