<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Dashboard</title>
    </head>
    <body id='body2'>
        <div class="grid-container">
            
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
			
            <main class="main">
            <div class="main-header">
                <div class="main-header__heading">
					
                </div>
                
            </div>
            <div class="main-overview">
                <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            </div>

            <div class="main-cards">
                <div class="card">
                
                </div>
                <div class="card">
                </div>
            </div>
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>
