<?php
session_start();
date_default_timezone_set("Europe/Paris");
$socket = fsockopen('unix:///var/run/docker.sock',0, $errno, $errstr,20);
if(!$socket) {
	echo "erreur" . $errno . " " . $errstr	;
}
else {
	$out = "GET /networks HTTP/1.1\r\n";
	$out .= "Host: \r\n";
	$out .= "Content-Type: application/json\r\n";
	$out .= "Connection: Close\r\n\r\n";
	fwrite($socket,$out);

	$header = "";
	$data = "";
	$isHeader = True;

	while(!feof($socket)) {
		$output = fgets($socket,2048); 		//On recupère ce qu'il y a dans le socket
		$output = str_replace(array("\n", "\r"), '',$output); //On elève les "\r" et "\n"


		if(substr($output, 0, 2) == "[{") 	// Si le début de la phrase est "[{", c'est le début des données à récuperer
			$isHeader = False;


		if($isHeader) 						//Si l'output est un header, on l'enregistre dans la varible header
			$header .= $output;
		else 								//Si l'output sont les données, on l'enregistre dans la varible data
			$data .= $output;
	}
	

	// $data{strlen($data)-1} Permet de récupérer le dernier caracère d'une string
	while($data{strlen($data)-1} ==! "]")
		$data = substr($data, 0, -1); //Suppression du dernier caractère

	$json = json_decode($data,true);
	fclose($socket);
//	echo "<pre>";
//	print_r($json);
//	echo "</pre>";
}?>

<?php 
if(isset($_SESSION['token']) && isset($_SESSION['username'])) {

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Network - MicroAir</title>
    </head>
    <body id='body2'>
        <div class="grid-container">
        <div hidden id='popup-infos'> </div>
            
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
            <main class="main">
            <div class="main-header conteneurs">
                <div class="main-header__heading">
                <div hidden id='popupCreateNetwork' class="__popup">
                <span class='cross'>&#10007;</span>
                <h1>Creer un network</h1>
                	<div class='formCreate'>
           			<label for='createnetworkId'>Nom du network <span class='required'>*</span></label>
           			<input type='text' name='createnetworkId' id='createnetworkId' placeholder='Nom du network'>
           			<label for='createIpRange'>IPRange <span class='required'>*</span><span class="example">(ex: _._._._/CIDR)</span> </label>
           			<input type='text' name='createIpRange' id='createIpRange' placeholder='172.20.0.0/16'>
           			<label for='createSubnet'>Sous reseau <span class='required'>*</span><span class="example">(ex: _._._._/CIDR)</span> </label>
           			<input type='text' name='createSubnet' id='createSubnet' placeholder='172.20.0.0/16'>
           			<label for='createGateway'>Passerelle <span class='required'>*</span><span class="example">(ex: _._._._)</span> </label>
           			<input type='text' name='createGateway' id='createGateway' placeholder='172.20.10.11'>
                	<button name="validerCreateNetwork">VALIDER</button>
                	</div> <!-- formCreate -->
                	<span class='textRequired'><span class='required'>*</span> obligatoire</span>
                </div>
                <div hidden id="popupConnectNetwork" class='__popup'>
                	<span class='cross'>&#10007;</span>
                	<h1>Connecter un conteneur au network : </h1>
                	<table border=0 cellspacing=0 cellpadding=0>
<!--                		<tr><td><input type="checkbox" name="NAME_OF_CONTENAIR"/></td><td>NOM CONTENEUR</td></tr>-->
                	</table>
                	<button id="validerConnection">VALIDER</button>
                </div>
                <div hidden id="popupDisconnectNetwork" class='__popup'> 
                	<span class='cross'>&#10007;</span>
                	<h1>Deconnecter un conteneur du network : </h1>
                	<table border=0 cellspacing=0 cellpadding=0>
                	
                	</table>
                	<button id='validerDeconnexion'>VALIDER</button>
                </div>
                <h1>Network</h1>
					<div id='gestion_network'>
					<div id='action_network'>
						<div>&#10010;</div><div>CONNECTER</div><div>DECONNECTER</div><div>&#10062;</div>
					</div>
						<table cellpadding=0 cellspacing=0>
							<tr><td></td><td>Nom</td><td>ID</td><td>IPRange</td><td>Sous réseau</td><td>Passerelle</td></tr>
							<?php 
//							echo "<pre>";
//							print_r($json);
//							echo "</pre>";
							for($i = 0 ; $i < sizeof($json) ; $i++) {
								echo "<tr>";
								$name = ltrim($json[$i]['Name'],'/'); 
								echo "<td><input type='checkbox' nb='{$i}' id='{$json[$i]['Id']}' name='{$json[$i]['Name']}' /></td>";
								echo "<td>{$json[$i]['Name']}</td>";
								echo "<td>{$json[$i]['Id']}</td>";
								echo "<td>{$json[$i]['IPAM']['Config'][0]['IPRange']}</td>";
								echo "<td>{$json[$i]['IPAM']['Config'][0]['Subnet']}</td>";
								echo "<td>{$json[$i]['IPAM']['Config'][0]['Gateway']}</td>";
				//				echo "<td>{$json[$i]['Status']}</td>";
								echo "</tr>";
								
							}
							
							
							?>
							
							
						</table>
					
					</div>
                </div>
                
            </div>
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>
<script>
	var boutons_action = document.querySelectorAll("#action_network div");
	var checkboxs = document.querySelectorAll("#gestion_network input[type=checkbox]");
//	// console.log(checkboxs);
	var popupConnectNetwork = document.getElementById("popupConnectNetwork");
	var popupCreateNetwork = document.getElementById("popupCreateNetwork");
	var popupDisconnectNetwork = document.getElementById("popupDisconnectNetwork");
	var buttonValidateCreateNetwork = document.querySelector("#popupCreateNetwork button[name=validerCreateNetwork]");
	var cross = popupConnectNetwork.querySelector(".cross");
	var crossCreateNetwork = document.querySelector("#popupCreateNetwork .cross");
	var crossDisconnectNetwork = document.querySelector("#popupDisconnectNetwork .cross");
//	// console.log(body);
	for(var i = 0 ; i < checkboxs.length ; i++) {
		checkboxs[i].checked = false;
	}
//	// console.log(boutons_action);
	var xhr = new XMLHttpRequest();
	var container_name;
	xhr.onreadystatechange = callback_gestionNetwork;
	var clicked = false;
	var clickedCreateNetwork = false;
	var clickedDisconnectNetwork = false;
	var networkId;
	var networkName;
	var nb = -1; // == ligne cochée , initialisation à -1
	for(i = 0 ; i < checkboxs.length ; i++) {
		checkboxs[i].addEventListener("click",function() {		
			/* Permet de ne gerer qu'un seul network  la fois, pour le moment on va faire simple */
			if(this.checked == true && clicked == false && nb == -1) {
				clicked = true;
				nb = this.getAttribute("nb");
//				// console.log(nb);
				boutons_action[0].style.color = "rgba(0,0,255,0.3)";
				boutons_action[1].style.backgroundColor = "green";
				boutons_action[2].style.backgroundColor = "indianred";
				boutons_action[0].style.cursor = "not-allowed";
				boutons_action[1].style.cursor = "pointer";
				boutons_action[2].style.cursor = "pointer";
				boutons_action[3].style.color = "indianred";
				boutons_action[3].style.cursor = "pointer";
				networkId = this.getAttribute("id");
				networkName = this.getAttribute("name");
				saveH1 = popupConnectNetwork.querySelector("h1");
				currentH1 = popupConnectNetwork.querySelector("h1");
				currentH1.innerHTML += networkId;
				// console.log(networkId);
				
			}
			else {
				clicked = false;
				nb = -1;
				boutons_action[0].style.color = "rgba(0,0,255,1)";
				boutons_action[0].style.cursor = "pointer";
				boutons_action[1].style.backgroundColor = "rgba(0,230,0,0.4)";
				boutons_action[2].style.backgroundColor = "rgba(255,69,0,0.4)";
				boutons_action[1].style.cursor = "not-allowed";
				boutons_action[2].style.cursor = "not-allowed";
				boutons_action[3].style.cursor = "not-allowed";
				boutons_action[3].style.color = "rgba(255,69,0,0.4)";
			}
			
			/* ********* FIN CLIC *******/
			
			
			/* deux boucles qui permettent de disabled ou non les cases une fois coche */
			if(nb != -1) {
				for(var j = 0 ; j < checkboxs.length ; j++) {
					if(j != nb) {
						checkboxs[j].disabled = true;
					}
				}
			}
			else {
				for(var j = 0 ; j < checkboxs.length ; j++) {
					if(j != nb) {
						checkboxs[j].disabled = false;
					}
				}
			}
			
		});
	}

boutons_action[0].addEventListener("click",function(e) { // bouton creation network
	if(clicked == false) { // pas de network selectionne
		popupCreateNetwork.style.display = "block";
		clickedCreateNetwork = true;
	}
});	
	
boutons_action[1].addEventListener("click",function(e) { // bouton connecter
	if(clicked == true && nb != -1 && clickedCreateNetwork == false && clickedDisconnectNetwork == false) {
		popupConnectNetwork.style.display = "block";
		xhr.open("POST","js/listeConteneurs.php");
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		to_send = "listContainer";
		xhr.send(to_send);
//			body2.style.backgroundColor = "lightgrey";	
	}
});

boutons_action[2].addEventListener("click",function(e) { // bouton deconnecter
	if(clicked == true && nb != -1 && clickedCreateNetwork == false && clickedDisconnectNetwork == false) {
		console.log(networkId);
		xhr.open("POST","js/network.php");
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		to_send = "networkName="+networkName+"&disconnect";
		xhr.send(to_send);
		popupDisconnectNetwork.style.display = "block";
		clickedDisconnectNetwork = true;
//		console.log(to_send);
	}
});

boutons_action[3].addEventListener("click",function(e) { // bouton supprimer
	if(clicked == true && nb != -1 && clickedCreateNetwork == false && clickedDisconnectNetwork == false) {
		if(confirm("Confirmer la suppression ?")) {
			xhr.open("POST","js/network.php");
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			to_send = "networkId="+networkId+"&delete";
			xhr.send(to_send);
//			console.log(to_send);
		}
		
	}
});

cross.addEventListener("click",function(e){
		popupConnectNetwork.style.display = "none";
		tableUpdate.innerHTML = "";
		nbContainerClicked = -1;
		clickedContainer = false;
//			clicked = false;
});

crossCreateNetwork.addEventListener("click",function(e) {
	popupCreateNetwork.style.display = "none";
	clickedCreateNetwork = false;
	nbContainerClicked = -1;
	clickedContainer = false;
});
	
crossDisconnectNetwork.addEventListener("click",function(e) {
	popupDisconnectNetwork.style.display = "none";
	clickedDisconnectNetwork = false;
	nbContainerClicked = -1;
	clickedContainer = false;
});	

//	var connectContainer = document.querySelectorAll(".connectContainer");
	
	
	
	
	var intervals = 0;
	var intervalId;
	var popup = document.getElementById("popup-infos");
	function fpopup() {
		if(intervals == 3) {
			clearInterval(intervalId);
			window.location.href = window.location;
			document.body.style.cursor = "pointer";
		}	
		popup.style.display = "block";
		popup.style.backgroundColor = "green";		
		intervals += 1;
	}
	
var tableUpdate;
var clickedContainer = false;
var idContainer;

var nbContainerClicked = -1;
var validerConnection = document.getElementById("validerConnection");
var validerDeconnexion = document.getElementById("validerDeconnexion");

function eventConnectContainer() {
	inputConnectContainer = document.querySelectorAll("#connectContainer");
	if(inputConnectContainer.length > 0) {
//			// console.log(connectContainer);		
		for(i = 0 ; i < inputConnectContainer.length ; i++) {
			inputConnectContainer[i].addEventListener("click",function(e){
				if(nbContainerClicked == -1 && clickedContainer == false) {
					idContainer = this.getAttribute("name");
					clickedContainer = true;
					nbContainerClicked = this.getAttribute("nbContainer");
//					// console.log(nbContainerClicked);
//					// console.log("true");
				}
				else {
					clickedContainer = false;
//					// console.log("false");
					nbContainerClicked = -1;
				}
				if(nbContainerClicked != -1) {
					for(j = 0 ; j < inputConnectContainer.length ; j++) {
						if(j != nbContainerClicked) {
							inputConnectContainer[j].disabled = true;	
						}	
					}
				}
				else {
					for(var j = 0 ; j < inputConnectContainer.length ; j++) {
						if(j != nbContainerClicked) {
							inputConnectContainer[j].disabled = false;
						}
					}
				}
			});
			
			
			
		}
	}
}

function eventDisconnectContainer() {
	inputDisconnectContainer = document.querySelectorAll("#disconnectContainer");
	if(inputDisconnectContainer.length > 0 ){
		for(i = 0 ; i < inputDisconnectContainer.length ; i++) {
			inputDisconnectContainer[i].addEventListener("click",function(e){
				if(nbContainerClicked == -1 && clickedContainer == false) {
					containerName = this.getAttribute("name");
//					console.log(containerName);
					clickedContainer = true;
					nbContainerClicked = this.getAttribute("nbContainer");
					
//					// console.log(nbContainerClicked);
//					// console.log("true");
				}
				else {
					clickedContainer = false;
//					// console.log("false");
					nbContainerClicked = -1;
				}
				if(nbContainerClicked != -1) {
					for(j = 0 ; j < inputDisconnectContainer.length ; j++) {
						if(j != nbContainerClicked) {
							inputDisconnectContainer[j].disabled = true;	
						}	
					}
				}
				else {
					for(var j = 0 ; j < inputDisconnectContainer.length ; j++) {
						if(j != nbContainerClicked) {
							inputDisconnectContainer[j].disabled = false;
						}
					}
				}
			});
			
			
			
		}
	}
}

buttonValidateCreateNetwork.addEventListener("click",function(e){
	if(clickedCreateNetwork == true) {
		inputCreateNetwork = document.querySelectorAll(".formCreate input");
		// console.log(inputCreateNetwork);
		xhr.open("POST","js/network.php");
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		to_send = inputCreateNetwork[0].getAttribute("name")+"="+inputCreateNetwork[0].value+"&"+inputCreateNetwork[1].getAttribute("name")+"="+inputCreateNetwork[1].value+"&"+inputCreateNetwork[2].getAttribute("name")+"="+inputCreateNetwork[2].value+"&"+inputCreateNetwork[3].getAttribute("name")+"="+inputCreateNetwork[3].value;
//		console.log(to_send);
		xhr.send(to_send);
	}
});

validerConnection.addEventListener("click",function(e) {
	if(clickedContainer == true && nbContainerClicked != -1) {
		xhr.open("POST","js/listeConteneurs.php");
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send("idContainer="+idContainer+"&idNetwork="+networkId);
//		console.log("idContainer="+idContainer+"&idNetwork="+networkId); 	
	}
});

validerDeconnexion.addEventListener("click",function(e) {
	if(clickedContainer == true && nbContainerClicked != -1) {
		xhr.open("POST","js/network.php");
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send("containerName="+containerName+"&networkId="+networkId+"&disconnect");
//		console.log("containerName="+containerName+"&networkId="+networkId+"&disconnect"); 	
	}
});
	
function callback_gestionNetwork() {
//	console.log(xhr);
	if(xhr.status == 200 && xhr.readyState == 4) {
		// console.log(xhr.responseText);
		if(xhr.responseText == "on") { // CONNECTER UN CONTENEUR A UN NETWORK
			if(confirm('Opération réussie')) {
				window.location.href = window.location;
			}
			else {
				window.location.href = window.location;
			}
		}
		else if(xhr.responseText == "off") { // DECONNECTER UN CONTENEUR A UN NETWORK
			if(confirm('Opération réussie')) {
				window.location.href = window.location;
			}
			else {
				window.location.href = window.location;
			}
		}
		else if(xhr.responseText == "create") {
			// console.log("createOK");
		}
		else if(xhr.responseText == "delete") {
			window.location.href = window.location;
		}
		else if(xhr.responseText.match("disconnect")) {
			res = JSON.parse(xhr.responseText);
			res.pop(); // permet de supprimer le disconnect,inutile dans nos donnees, mais utile pour check :)
			tableUpdate = document.querySelector("#popupDisconnectNetwork table");
			tableUpdate.innerHTML = "";
			for(i = 0 ; i < res.length ; i++) {
				tableUpdate.innerHTML += "<tr><td><input id='disconnectContainer' nbContainer='"+i+"' name='"+res[i]+"' type='checkbox'></td><td>"+res[i]+"</td>";
			}
			eventDisconnectContainer();
		}
		else { // LISTER LES CONTENEURS DANS LE POPUP "CONNECTER"
			res = JSON.parse(xhr.responseText);
//			console.log(res.length);

//			console.log(xhr.responseText);
			tableUpdate = document.querySelector("#popupConnectNetwork table");
			for(i = 0 ; i < res.length ; i++) {
				tableUpdate.innerHTML += "<tr><td><input nbContainer='"+i+"' id='connectContainer' type='checkbox' name='"+res[i]["Id"]+"'/></td><td>"+res[i]["Names"][0]+"</td></tr>";
				
		
			}
			eventConnectContainer();
		}
		
//			else if(xhr.responseText == "delete") {
//				intervalId = setInterval(fpopup, 100);
//				popup.innerHTML = "Conteneur supprimé avec succès";
//				
//			}
//				window.location.href = window.location;
		
		
		
	}
	if(xhr.responseURL == "http://localhost/projet-docker/dashboard/js/network.php" && xhr.readyState == 4 && !xhr.responseText.match("disconnect")) {
		window.location.href = window.location;
	}
}
	

</script>
<?php 


}
else {
	header("Location: login.php");
}
?>

