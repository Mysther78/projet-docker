<?php
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL); 

if(!(isset($_SESSION['token']) && isset($_SESSION['username']))) {
	header("Location: login.php");
}

		
if(isset($_POST["submit_creation_docker"]) && ($_POST["submit_creation_docker"] == "Création" && $_POST["nom_docker"] != "" && $_POST["type_docker"] != "")) {

	//Set des variables importantes pour la création et le build du dockerfile
	$dashboard = dirname($_SERVER['SCRIPT_FILENAME']);
	$directory_user = $dashboard . "/app/users/{$_SESSION['username']}";
	$path_docker = $directory_user."/docker";
	$csv_exec = $dashboard . "/app/exec.csv";
	//echo "directory user : ". $directory_user;
	//echo "path_docker : " . $path_docker;
	//echo "csv : " . $csv_exec;


	//Création du path pour la création du dockerfile (le dockerfile doit être dans un dossier à part et seul pour un fonctionnement correct)
	if(!is_dir($path_docker)) {
    		mkdir($path_docker,0755,True);
	}


	//Get sql file
	if(isset($_FILES["inputSql"]) && $_FILES["inputSql"] != "") {
		$uploadfile = $path_docker . "/" . time() . "_" . basename($_FILES['inputSql']['name']);

//w		echo '<pre>';
		if (move_uploaded_file($_FILES['inputSql']['tmp_name'], $uploadfile)) {
//		    echo "Le fichier est valide, et a été téléchargé
//		           avec succès. Voici plus d'informations :\n";
		} else {
		    echo "Attaque potentielle par téléchargement de fichiers.
		          Voici plus d'informations :\n";
		}

/*		echo 'Voici quelques informations de débogage :';
		print_r($_FILES);

		echo '</pre>';*/
	}

	if(($handle = fopen($path_docker . "/Dockerfile", "w")) == True) { 			// Ouverture du ficher 
		if($_POST["type_docker"] == "apache") {
			fputs($handle, 'FROM ubuntu:18.04'.PHP_EOL);
			fputs($handle, 'ENV DEBIAN_FRONTEND=noninteractive'.PHP_EOL);
			fputs($handle, 'RUN apt-get update && apt-get install -yq --no-install-recommends \
			apt-utils \
			curl \
			git \
			apache2 \
			libapache2-mod-php7.2 \
			php7.2-cli \
			php7.2-json \
			php7.2-curl \
			php7.2-fpm \
			php7.2-gd \
			php7.2-ldap \
			php7.2-mbstring \
			php7.2-mysql \
			php7.2-soap \
			php7.2-sqlite3 \
			php7.2-xml \
			php7.2-zip \
			php7.2-intl \
			php-imagick \
			openssl \
			nano \
			graphicsmagick \
			imagemagick \
			ghostscript \
			mysql-client \
			iputils-ping \
			locales \
			sqlite3 \
			ca-certificates \
	    	&& apt-get clean && rm -rf /var/lib/apt/lists/*'.PHP_EOL);
			fputs($handle, 'RUN apt-get install -y php-curl*'.PHP_EOL);
			fputs($handle, 'RUN apt-get install -y git'.PHP_EOL);
			fputs($handle, 'RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer'.PHP_EOL);
			fputs($handle, 'RUN locale-gen en_US.UTF-8 en_GB.UTF-8 de_DE.UTF-8 es_ES.UTF-8 fr_FR.UTF-8 it_IT.UTF-8 km_KH sv_SE.UTF-8 fi_FI.UTF-8'.PHP_EOL);
			fputs($handle, 'EXPOSE 80'.PHP_EOL);
			fputs($handle, 'WORKDIR /var/www/html'.PHP_EOL);
			fputs($handle, 'RUN rm index.html'.PHP_EOL);
			
			if($_POST["pseudo_git"] != "" && $_POST["mdp_git"] != "") {
				$lien_git = preg_split("#://#", $_POST["lien_git"], 2)[1];
				fputs($handle, 'RUN git clone https://'. $_POST["pseudo_git"] .':' . $_POST["mdp_git"]  . '@' . $lien_git .PHP_EOL);
			}
			else if($_POST["lien_git"] != ""){
				fputs($handle, 'RUN git clone ' . $_POST["lien_git"] .PHP_EOL);
			}

			if($_POST["lien_git"] != "") {
				$array_git = preg_split("#/#", $_POST["lien_git"]);
				$fin_lien_git = end($array_git);
				$nom_dossier_git = preg_split("/[.]/", $fin_lien_git)[0]; // Recuperation du nom de l'application git
				fputs($handle, 'RUN mv ' . $nom_dossier_git . '/* . '.PHP_EOL);
				fputs($handle, 'RUN ["rm", "-R", "' . $nom_dossier_git . '"]'.PHP_EOL);
			}

			fputs($handle, 'WORKDIR /var/www'.PHP_EOL);
			fputs($handle, 'RUN ["chown", "-R", "www-data:www-data" , "html"]'.PHP_EOL);

			fputs($handle, 'CMD apachectl -D FOREGROUND');
		}
		else if($_POST["type_docker"] == "postgres") {
			fputs($handle, 'FROM postgres:latest'.PHP_EOL);
			if($_FILES["inputSql"] != "") {
				fputs($handle, 'COPY *.sql /docker-entrypoint-initdb.d/'.PHP_EOL);
				fputs($handle, 'ENV POSTGRES_PASSWORD ');
				fputs($handle, isset($_POST["database_password"])?$_POST["database_password"]:"root");
				fputs($handle, PHP_EOL);
				fputs($handle, 'EXPOSE 5432'.PHP_EOL);
			}
		}
		else if($_POST["type_docker"] == "mysql") {
			fputs($handle, 'FROM mysql:5.7'.PHP_EOL);
			if($_FILES["inputSql"] != "") {
				fputs($handle, 'COPY *.sql /docker-entrypoint-initdb.d/'.PHP_EOL);
				fputs($handle, "ENV MYSQL_ROOT_PASSWORD ");
				fputs($handle, isset($_POST["database_password"])?$_POST["database_password"]:"root");
				fputs($handle, PHP_EOL);
				fputs($handle, 'EXPOSE 3306'.PHP_EOL);
			}
		}
		
		fclose($handle);


		//Début de l'écriture dans le fichier CSV pour l'execution 
		if(($handle_csv = fopen($csv_exec, "a")) == True) {
			fputcsv($handle_csv, array("$dashboard", $_SESSION['username'], $_POST["nom_docker"]), ";");
			fclose($handle_csv);
		}
		chdir("{$dashboard}/app");
		shell_exec("./build.sh  2>/dev/null >/dev/null &");
		
		echo "<script>
		alert('Création réussie avec succès');		
		</script>";
		
	}
	else{
		echo "Impossible d'ouvrir le fichier Dockerfile";
	}
}?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Création - MicroAir</title>
    </head>
    <body id='body2'>
        <div class="grid-container">          
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
            <main class="main">
		        <div class="main-header creation" >
		        <h1>CREATION D'IMAGES DOCKER</h1><hr>
		            <div class="main-header__heading">
		            
						<div id='main_div_creation'>
						<form enctype="multipart/form-data" method='POST' onsubmit="return verifForm(this)">
							<div><label for='nom_docker'>Nom de l'image</label><input type='text' name='nom_docker' placeholder="Nom de l'image" required autofocus></div>
							<div>
								<label for='type_docker'>Type d'image</label>
								<select name='type_docker'>
									<option value='apache'>Web (Apache)</option>
									<option value='mysql'>Base de données (MySQL)</option>
									<option value='postgres'>Base de données (Postgres)</option>
								</select>
							</div>
							<div class='choix'></div>
<!--						<div hidden class='__class_apache'>
								<div class='linkGit'><label for='lien_git'>Lien git</label><input type='text' name='lien_git' placeholder='Lien git'></div>
								<div class='pseudoGit'><label for='pseudo_git'>Pseudo git</label><input type='text' name='pseudo_git' placeholder='Pseudo git'></div>
								<div class='passwordGit'><label for='mdp_git'>Mot de passe</label><input type='password' name='mdp_git' placeholder='Mot de passe'></div>
							</div>
							<div class='__class_mysql'>
								<div class='fileSql'><label for='sql'>Fichier .sql à importer</label><input type='file' name='inputSql' id='sql' accept='.sql'/></div>					
							</div>
							<div class='__class_postgres'>
								<div class='fileSql'><label for='sql'>Fichier .sql à importer</label><input type='file' name='inputSql' id='sql' accept='.sql'/></div>					
							</div>-->
							
							<input type='submit' name='submit_creation_docker' value='Création'>
						</form>
						</div>
		            </div>      
		        </div>
            
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>


<script>

var selectTypeImage = document.querySelector("select[name=type_docker]");
var formulaireCreationImage = document.querySelector(".choix");


formulaireCreationImage.innerHTML = ""; // INITIALISATION 
formulaireCreationImage.innerHTML = "<div hidden class='__class_apache'><div class='linkGit'><label for='lien_git'>Lien git de l'application Web</label><input type='text' name='lien_git' placeholder='Lien git' required></div>";
formulaireCreationImage.innerHTML += "<div class='pseudoGit'><label for='pseudo_git'>Pseudo git</label><input type='text' name='pseudo_git' placeholder='Pseudo git'></div>";
formulaireCreationImage.innerHTML += "<div class='passwordGit'><label for='mdp_git'>Mot de passe</label><input type='password' name='mdp_git' placeholder='Mot de passe'></div></div>";					

selectTypeImage.addEventListener("change",function(e) { // A CHAQUE CHANGEMENT 
	if(selectTypeImage.value == "apache") {
		formulaireCreationImage.innerHTML = "";
		formulaireCreationImage.innerHTML = "<div hidden class='__class_apache'><div class='linkGit'><label for='lien_git'></label><input type='text' name='lien_git' placeholder='Lien git' required></div>";
		formulaireCreationImage.innerHTML += "<div class='pseudoGit'><label for='pseudo_git'>Pseudo git</label><input type='text' name='pseudo_git' placeholder='Pseudo git'></div>";
		formulaireCreationImage.innerHTML += "<div class='passwordGit'><label for='mdp_git'>Mot de passe</label><input type='password' name='mdp_git' placeholder='Mot de passe'></div></div>";					
	}
	else if(selectTypeImage.value == "mysql") {
		formulaireCreationImage.innerHTML = "";
		formulaireCreationImage.innerHTML += "<div class='__class_mysql'><div class='fileSql'><label for='sql'>Fichier .sql à importer</label><input type='file' name='inputSql' id='sql' accept='.sql'/></div></div>";
		formulaireCreationImage.innerHTML += "<div class='database_password'><label for='database_password'>Mot de passe admin MySQL</label><input type='text' name='database_password' placeholder='Mot de passe admin MySQL' required></div>";	
	}
	else if(selectTypeImage.value == "postgres") {
		formulaireCreationImage.innerHTML = "";
		formulaireCreationImage.innerHTML += "<div class='__class_mysql'><div class='fileSql'><label for='sql'>Fichier .sql à importer</label><input type='file' name='inputSql' id='sql' accept='.sql'/></div></div>";
		formulaireCreationImage.innerHTML += "<div class='database_password'><label for='database_password'>Mot de passe admin Postgres</label><input type='text' name='database_password' placeholder='Mot de passe admin Postgres' required></div>";	
	}
});

console.log(selectTypeImage.value);

</script>


