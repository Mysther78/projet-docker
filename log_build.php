<?php
session_start();
?>
<?php 
if(isset($_SESSION['token']) && isset($_SESSION['username'])) {

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Logs - MicroAir</title>
    </head>
    <body id='body2'>
        <div class="grid-container">
            
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
            

            

            <main class="main">
          <div class="main-header">
                <div class="main-header__heading">
                
                <h3>Logs de tous les builds</h3>
                </div>
                
            </div>

<!--            <div class="main-overview">
                <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            <div class="overviewcard">
                <div class="overviewcard__icon">Overview</div>
                <div class="overviewcard__info">Card</div>
            </div>
            </div>
-->
		
            <div class="main-cards">
                
                <?php 
                chdir("app/users");
                $cmd = shell_exec("ls");
                
				$tok = strtok($cmd, " \n\t");
				$hasLog = false;
# On check si la personne a bien un dossier log 
				while ($tok !== false) { 
					if($_SESSION['username'] == $tok) {
						$hasLog = true;
					}
					$tok = strtok("\n\t");
				}    
                
                if($hasLog == true) {
                	chdir("{$_SESSION['username']}"); #on change le repertoire courant
                	$ls = shell_exec("ls");
                	$log = strtok($ls,"\n\t");
                	$logsName = []; #tableau avec tout le nom de logs fichiers a ouvrir
                	while($log !== false) {
                		if(preg_match("#.log#",$log)) { #on recupere bien tout les fichiers qui se finissent par .log
                			$logsName[] = $log;
                		}
                		$log = strtok("\n\t");
                	}
                	if(sizeof($logsName) > 0) { #petit check des familles
                		for($numLog = 0 ; $numLog < sizeof($logsName) ; $numLog++) { #on parcours tout les logs 
                			$handle = fopen($logsName[$numLog], "r");
							if ($handle) {
								echo "<div class='card'>";
								echo "<h3>{$logsName[$numLog]} <div class='supprimer-log'>&#10006;</div> </h3>";
								while (($buffer = fgets($handle, 2048)) !== false) {
									echo $buffer . "<br>";
								}
								if (!feof($handle)) {
									echo "Erreur: fgets() a échoué\n";
								}
								fclose($handle);
								echo "</div>";
							}
                		}
                	}
                	else {
                		chdir("../../");
                		$erreur = "Vous n'avez pas d'historique de build";
               		}	  		
                }
                else {
                	chdhir("../../");
                	$erreur = "Vous n'avez pas d'historique de build";
                }	
                if(isset($erreur)) {
                	echo "<div class='card'>{$erreur}</div>";
                }
                ?>
                
            </div>
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>

<script>

var deleteLog = document.querySelectorAll(".supprimer-log");
var xhr = new XMLHttpRequest();
xhr.onreadystatechange = callback_deleteLog;
for(i = 0 ; i < deleteLog.length ; i++) {
	deleteLog[i].addEventListener("click",function(e) {
		if(confirm("Confirmer la suppression ?")) {
			toSplit = e.target.parentNode.textContent;
			xhr.open("POST","js/log.php");
			var logName = toSplit.split(" ");
			console.log(logName[0]);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send("logName="+logName[0]);
		}
	});
}

function callback_deleteLog() {
	console.log(xhr);
	if(xhr.status == 200 && xhr.readyState == 4) {
		alert("Supprimé avec succès");
		window.location.href = window.location;
	}
}


</script>


<?php 
}
else {
	header("Location: login.php");
}
?>
