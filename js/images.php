<?php

if(isset($_POST['idImage'])) {
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "http://v1.39/images/{$_POST['idImage']}?force=true");
	curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
	echo $result;

}


?>
