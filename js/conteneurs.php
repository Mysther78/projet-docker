<?php

if(isset($_POST['container_name'])) { // on verifie si la variable est bien passee
	if(isset($_POST['start'])) { // on traite tout les choix un par un, ici on veut start le conteneur
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://v1.39/containers/{$_POST['container_name']}/start");
		curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = array('Host: ','Content-Type: application/json');		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		echo "on";
	}
	else if(isset($_POST['stop'])) { // on traite tout les choix un par un, ici on veut start le conteneur
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://v1.39/containers/{$_POST['container_name']}/stop");
		curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = array('Host: ','Content-Type: application/json');		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		echo "off";
	}
	else if(isset($_POST['delete'])) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://v1.39/containers/{$_POST['container_name']}");
		curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		echo "delete";

	}
}


?>
