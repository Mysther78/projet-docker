<?php 
	
if(isset($_POST['createnetworkId']) && isset($_POST['createGateway']) && isset($_POST['createIpRange']) && isset($_POST['createSubnet'])) {
	$ch = curl_init();
	$networkName = htmlspecialchars($_POST['createnetworkId']);
	$IPRange = htmlspecialchars($_POST['createIpRange']);
	$subnet = htmlspecialchars($_POST['createSubnet']);
	$gateway = htmlspecialchars($_POST['createGateway']);
	curl_setopt($ch, CURLOPT_URL, 'http://v1.39/networks/create');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"Name\":\"{$networkName}\",\"IPAM\":{\"Config\":[{\"Subnet\":\"{$subnet}\",\"IPRange\":\"{$IPRange}\",\"Gateway\":\"{$gateway}\"}]}}");
	curl_setopt($ch, CURLOPT_POST, 1);

	$headers = array();
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	echo "create";
	curl_close ($ch);
	
	
}
else if(isset($_POST['delete']) && isset($_POST['networkId']) ) {
	$ch = curl_init();
	$networkId = htmlspecialchars($_POST['networkId']);
	curl_setopt($ch, CURLOPT_URL, "http://v1.39/networks/{$networkId}");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

	$headers = array();
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	
	curl_close ($ch);
	echo "delete";
}
else if(isset($_POST['disconnect']) && isset($_POST['networkName'])) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'http://v1.39/containers/json?all=1');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");

	$headers = array();
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
	$json = json_decode($result,true);

	$networkName = $_POST['networkName'];
	$containersConnected = [];
	for($i = 0 ; $i < sizeof($json) ; $i++) {
		if(isset($json[$i]["NetworkSettings"]["Networks"][$networkName])) {
			$containersConnected[] = ltrim($json[$i]['Names'][0],"/");
		}
	}
	$containersConnected[] = "disconnect";
//	echo "okeoapdkpfs";
	echo json_encode($containersConnected);

}
else if(isset($_POST['disconnect']) && isset($_POST['containerName'])) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,"http://v1.39/networks/{$_POST['networkId']}/disconnect");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"Container\":\"{$_POST['containerName']}\"}");
	curl_setopt($ch, CURLOPT_POST, 1);

	$headers = array();
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close ($ch);
	echo "off" . $result;
}


?>
