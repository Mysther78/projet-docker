const menuIconEl = $('.menu-icon');
const sidenavEl = $('.sidenav');
const sidenavElLi = $('.sidenav li');

const sidenavCloseEl = $('.sidenav__close-icon');
// Add and remove provided class names
function toggleClassName(el, className) {
  if (el.hasClass(className)) {
    el.removeClass(className);
  } else {
    el.addClass(className);
  }
}

// for(var i = 0 ; i < sidenavElLi.length ; i++) {
//     sidenavElLi[i].addEventListener("click",function(e) {
//         console.log(e.target);
//         if(this != e.target) {
//             this.removeClass('active');
//         }
//         else {
//             this.classList.add('active');
//         }
//
//     });
// }


// Open the side nav on click
menuIconEl.on('click', function(e) {
  toggleClassName(sidenavEl, 'active');
});

// Close the side nav on click
sidenavCloseEl.on('click', function() {
  toggleClassName(sidenavEl, 'active');
});
