<?php 
session_start();
if(isset($_SESSION['token']) && isset($_SESSION['username'])) {

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/stylesheet.css" />
        <?php include "include/font.html"?>
        

        <title>Gestion des images - MicroAir </title>
    </head>
    <body id='body2'>
    <div id='popup'>
		<div id='popup_creation'>
			<h1>Créer un conteneur à partir de l'image : </h1>
			<span class='cross'>&#10007;</span> 
			<form method='post' action=''>
			<label for='nom'>Nom du conteneur</label>
			<input type='text' id='nom' name='nom_conteneur' placeholder='Nom'/>
			<label for='portPublic'>Port public</label>
			<input type='text' id='portPublic' name='publicPort' placeholder='Port public'/>
			<label for='portPrive'>Port privé</label>
			<input type='text' id='portPrive' name='privatePort' placeholder='Port privée'/>
			<input type='text' id='imageNameInPost' name='imageName' hidden />
			<input type='submit' name='submit_creation_conteneur' value='CREATION'/>
			</form>
		</div>
	</div>
        <div class="grid-container">
            
			<?php 
			include "include/header.php"; 
			include "include/aside.php";
			?>
            <main class="main">
            
            <div class="main-header images">
            <h1>GESTION DES IMAGES DOCKER</h1><hr>
                <div class="main-header__heading">
					<?php
//				if(isset($_SESSION['token'])) {
					$socket = fsockopen('unix:///var/run/docker.sock',0, $errno, $errstr,20);
					if(!$socket) {
						echo "erreur" . $errno . " " . $errstr	;
					}
					else {
						$out = "GET /images/json HTTP/1.1\r\n";
						$out .= "Host: \r\n";
						$out .= "Content-Type: application/json\r\n";
						$out .= "Connection: Close\r\n\r\n";
						fwrite($socket,$out);

						$header = "";
						$data = "";
						$isHeader = True;

						while(!feof($socket)) {
							$output = fgets($socket,1024); 		//On recupère ce qu'il y a dans le socket
							$output = str_replace(array("\n", "\r"), '',$output); //On elève les "\r" et "\n"


							if(substr($output, 0, 2) == "[{") 	// Si le début de la phrase est "[{", c'est le début des données à récuperer
								$isHeader = False;


							if($isHeader) 						//Si l'output est un header, on l'enregistre dans la varible header
								$header .= $output;
							else 								//Si l'output sont les données, on l'enregistre dans la varible data
								$data .= $output;
						}

						fclose($socket);
						// $data{strlen($data)-1} Permet de récupérer le dernier caracère d'une string
						while($data{strlen($data)-1} ==! "]"  and strlen($data) > 0)
							$data = substr($data, 0, -1); //Suppression du dernier caractère

						$json = json_decode($data,true);
	//					echo "<pre>";
	//					print_r($json);
	//					echo "</pre>";
						for($i = 0 ; $i < sizeof($json) ; $i++) {
							//echo $json[$i]["RepoTags"][0] . " " . date("d/m/Y à h:i:s",$json[$i]["Created"]) . "<br>";
							echo "<div id='main-container-liste-docker'>";
							echo "<div class='element-docker'>" . "<div id='gauche'>";
							echo "<div hidden class='title'>" . $json[$i]["Id"] . "</div>";
							echo "<div class='repo-tag'>" . $json[$i]["RepoTags"][0] . "</div>";
							echo "<div class='label-local'>Local</div>";
							echo "<div class='last_update'>Crée le " . date("d/m/Y à H:i:s",$json[$i]["Created"]) . "</div>";
							echo "</div> <!-- FIN GAUCHE --->";
							echo "<div id='droite'>";
							echo "<div class='download'><span nb='{$i}' class='info'>&#8505;</span><span nb='{$i}' class='supprimer-image'>&#10006;</span></div>";
							echo "</div> <!-- FIN DROITE --->";
							echo "</div> <!-- FIN ELEMENT DOCKER --->";
							echo "</div> <!-- FIN CONTAINER --->";
						}
						// il nous faut : 
						// le nom , date de creation , le sa taille -> VirtualSize 
						
					}
					

					if(isset($_SESSION["token"])) {				
						echo "<div id='loading'>Chargement des images du DockerHub ... </div>"; // FAIRE UN BEAU CSS APRES ICI
						echo "<script src='listeDocker.js'></script>";
					}
				else {
						?> <!-- MESSAGE NON CONNECTE --->
						<p>Oops, il semblerait que vous n'etes pas connecté, rendez vous sur la <a href="compte.php">page de connexion</a> pour avoir accès à votre liste d'image sur DockerHub !</p>
						<?php
					}
				
				
			?>
                </div>
            </div>
            </main>

           <?php include "include/footer.html"; ?>
        </div>
    </body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src='js/index.js'></script>
<script>
	var btInfo = document.querySelectorAll(".info");
	xhrDelete = new XMLHttpRequest();
	xhrDelete.open('POST','js/images.php');
	xhrDelete.onreadystatechange = callback_delete;
	var imageTitle = document.querySelectorAll(".title");
	var popup = document.getElementById("popup");
	var popup_creation = document.getElementById("popup_creation");
	var cross = popup_creation.querySelector("span[class=cross]");
	var supprimer_image = document.querySelectorAll(".supprimer-image");
	console.log(supprimer_image);
	var h1 = popup_creation.querySelector("h1");
	var inputImageName = popup_creation.querySelector("input[id=imageNameInPost]");
	var saveH1 = h1.innerHTML;
//	console.log(popup_creation.childNodes);
	var i;
	var saveX, saveY;
	for(i = 0 ; i < btInfo.length ; i++) {
		btInfo[i].addEventListener("click",function(e) {
			popup.style.display = "block";
			saveX = window.pageXOffset;
			saveY = window.pageYOffset;
			window.scrollTo(0,0);
			console.log(e.target);
			h1.innerHTML = saveH1 + "<font color='red'>"+imageTitle[e.target.getAttribute("nb")].innerHTML + "</font>";
			inputImageName.value = imageTitle[e.target.getAttribute("nb")].innerHTML;
		});
	}
	cross.addEventListener("click",function(e){ // span cross
		console.log(e.target);
		popup.style.display = "none";
		window.scrollTo(saveX,saveY);
	});	
	for(i = 0 ; i < supprimer_image.length ; i++) {
		supprimer_image[i].addEventListener("click",function(e) {
			if(confirm("Supprimer l'image : "+imageTitle[e.target.getAttribute("nb")].innerHTML + "")) {
				idImage = imageTitle[e.target.getAttribute("nb")].innerHTML;
				console.log(idImage);
				
				
				
				xhrDelete.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhrDelete.send("idImage="+idImage);
			}
		});
	}
	

function callback_delete() {
	console.log(xhrDelete);
	if(xhrDelete.status == 200 && xhrDelete.readyState == 4) {
		
		window.location.href = window.location;
		
	}
}	

</script>


<?php


//curl --silent --unix-socket /var/run/docker.sock "http://v1.39/containers/create?name=testmdr" -X POST -H "Content-Type: application/json" -d '{"Image":"hello-world"}' 
if(isset($_POST['submit_creation_conteneur'])) {
	if(!empty($_POST['nom_conteneur']) && !empty($_POST['imageName'])) {
		$containerName = htmlspecialchars($_POST['nom_conteneur']);
		$imageName = $_POST['imageName'];
		$publicPort = htmlspecialchars($_POST['publicPort']);
		$privatePort = htmlspecialchars($_POST['privatePort']);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://v1.39/containers/create?name={$containerName}");
		curl_setopt($ch, CURLOPT_UNIX_SOCKET_PATH,"/var/run/docker.sock");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(($privatePort != "") && ($publicPort != "")) {
			curl_setopt($ch,CURLOPT_POSTFIELDS, "{\"Image\":\"{$imageName}\",\"ExposedPorts\":{\"{$privatePort}/tcp\":{}},\"PortBindings\":{\"{$privatePort}/tcp\":[{\"HostPort\":\"{$publicPort}\"}]}}");
		}
		else {
			curl_setopt($ch,CURLOPT_POSTFIELDS, "{\"Image\":\"{$imageName}\"}");
		}
		
		curl_setopt($ch, CURLOPT_POST, 1);
		$headers = array('Host: ','Content-Type: application/json');		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		echo $result;
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		
		echo "<script>alert('Conteneur créé avec succès');
		window.location.href = window.location;</script>";
	}
}


?>

<?php 
}
else {
	header("Location: login.php");
}
?>

